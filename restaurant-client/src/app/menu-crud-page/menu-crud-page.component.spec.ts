import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCrudPageComponent } from './menu-crud-page.component';

describe('MenuCrudPageComponent', () => {
  let component: MenuCrudPageComponent;
  let fixture: ComponentFixture<MenuCrudPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuCrudPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCrudPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
