import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';
import { ItemService } from '../item.service';
import { Category } from '../model/category.model';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-menu-crud-page',
  templateUrl: './menu-crud-page.component.html',
  styleUrls: ['./menu-crud-page.component.css']
})
export class MenuCrudPageComponent implements OnInit {
  items:Item[];
  categories:Category[];
  itemToEdit:Item;
  isForEdit:boolean;

  public last=false;
  public page=0;
  public size=5;

  constructor(
    private itemService:ItemService,
    private categoryService:CategoryService
  ) { 
    this.items=[];
    this.categories=[];
    this.isForEdit=false;
  }

  ngOnInit(): void {
    this.getItems();
    this.getCategories();
  }
  resetItemToEdit(){
    this.itemToEdit=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
  }
  getItems(){
    this.itemService.getItemPaged(this.page,this.size)
    .subscribe(res=>
      {this.items=res.content;
      this.last=res.last});
  }
  delteItem(id:number){
    this.itemService.deleteItemById(id)
    .subscribe(_=>this.getItems());
  }
  getCategories(){
    this.categoryService.getCategories()
    .subscribe(res=>this.categories=res);
  }
  saveItem(newItem: Item){
    this.itemService.saveItem(newItem)
    .subscribe(_=>this.getItems());
  }
  setItemToEdit(item:Item){
    this.itemToEdit=item;
    this.isForEdit=true;
  }
  findItemByName(name:string){
    this.itemService.findItemByName(name)
    .subscribe(res=>this.items=res)
  }
  setAppropriateForm(){
    this.isForEdit=false;
  }
  editItem(item:Item){
    this.itemService.updateItem(item)
    .subscribe(_=>this.getItems());
  }
  nextPage(){
    this.page++;
    this.getItems();
  }
  previousPage(){
    this.page--;
    this.getItems();
  }
  changePageSize(){
    this.page=0;
    this.getItems();
  }
}
