import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './error-page/error-page.component';
import { MenuCrudPageComponent } from './menu-crud-page/menu-crud-page.component';
import { MenuPageComponent } from './menu-page/menu-page.component';

const routes: Routes = [
  {path:'menu-crud',component:MenuCrudPageComponent},
  {path:'menu',component:MenuPageComponent},
  {path:'',redirectTo:'menu',pathMatch:'full'},
  {path:'**',component:ErrorPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
