import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Category } from '../model/category.model';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  itemToAdd:Item;
  @Input()
  categories:Category[];

  @Output()
  ItemAdded : EventEmitter<Item> =new EventEmitter();

  constructor() { 
    this.resetItemToAdd();
    this.categories=[];
  }

  ngOnInit(): void {
  }
  saveItem(){
    this.ItemAdded.next(this.itemToAdd);
    this.resetItemToAdd();
  }
  resetItemToAdd(){
    this.itemToAdd=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
  }
  resetItem(){
    this.resetItemToAdd();
  }
}
