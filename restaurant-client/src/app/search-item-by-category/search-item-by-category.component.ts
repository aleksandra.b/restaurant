import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search-item-by-category',
  templateUrl: './search-item-by-category.component.html',
  styleUrls: ['./search-item-by-category.component.css']
})
export class SearchItemByCategoryComponent implements OnInit {
  public categoryName:string;

  @Output()
  CategoryNameAdded : EventEmitter<string> =new EventEmitter();

  constructor() {
    this.categoryName='';
   }

  ngOnInit(): void {
  }
  findItemsByCategory(){
    this.CategoryNameAdded.next(this.categoryName);
  }
}
