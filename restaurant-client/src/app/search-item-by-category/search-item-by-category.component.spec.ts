import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemByCategoryComponent } from './search-item-by-category.component';

describe('SearchItemByCategoryComponent', () => {
  let component: SearchItemByCategoryComponent;
  let fixture: ComponentFixture<SearchItemByCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchItemByCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemByCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
