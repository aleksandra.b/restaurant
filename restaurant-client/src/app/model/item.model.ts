import { Category } from "./category.model";

interface ItemInterface{
    id?:number;
    name:string;
    price:number;
    category:Category;
}
export class Item implements ItemInterface{
    id?: number | undefined;
    name: string;
    price: number;
    category: Category;
    
    constructor(obj:ItemInterface){
        this.id=obj.id;
        this.name=obj.name;
        this.price=obj.price;
        this.category=obj.category;
    }
}