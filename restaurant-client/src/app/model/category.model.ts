interface CategoryInterface{
    id?:number;
    name:string;
}
export class Category implements CategoryInterface{
    id?: number | undefined;
    name: string;
    constructor(obj:CategoryInterface){
        this.id=obj.id;
        this.name=obj.name;
    } 
}