import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from "@angular/common";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuPageComponent } from './menu-page/menu-page.component';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { SearchItemByCategoryComponent } from './search-item-by-category/search-item-by-category.component';
import { MenuCrudPageComponent } from './menu-crud-page/menu-crud-page.component';
import { ItemListForEditComponent } from './item-list-for-edit/item-list-for-edit.component';
import { SearchItemByNameComponent } from './search-item-by-name/search-item-by-name.component';
import { ItemEditComponent } from './item-edit/item-edit.component';
import { FormComponent } from './form/form.component';
import { EditFormComponent } from './edit-form/edit-form.component';
import { ErrorPageComponent } from './error-page/error-page.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuPageComponent,
    ItemComponent,
    ItemListComponent,
    SearchItemByCategoryComponent,
    MenuCrudPageComponent,
    ItemListForEditComponent,
    SearchItemByNameComponent,
    ItemEditComponent,
    FormComponent,
    EditFormComponent,
    ErrorPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
