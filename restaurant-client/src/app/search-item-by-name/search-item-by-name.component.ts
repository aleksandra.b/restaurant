import { Component, OnInit ,Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-search-item-by-name',
  templateUrl: './search-item-by-name.component.html',
  styleUrls: ['./search-item-by-name.component.css']
})
export class SearchItemByNameComponent implements OnInit {
  public name:string;

  @Output()
  NameAdded : EventEmitter<string> =new EventEmitter();
  constructor() { 
    this.name='';
  }

  ngOnInit(): void {
  }
  findItemsByName(){
    this.NameAdded.next(this.name);
  }
}
