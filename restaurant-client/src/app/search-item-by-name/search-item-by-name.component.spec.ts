import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchItemByNameComponent } from './search-item-by-name.component';

describe('SearchItemByNameComponent', () => {
  let component: SearchItemByNameComponent;
  let fixture: ComponentFixture<SearchItemByNameComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchItemByNameComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchItemByNameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
