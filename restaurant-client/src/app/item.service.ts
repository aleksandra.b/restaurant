import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemPage } from './model/item-page.model';
import { Item } from './model/item.model';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  urlItems="http://localhost:8080/api/items";

  constructor(private http:HttpClient) { }
  getItems():Observable<Item[]>{
    return this.http.get<Item[]>(this.urlItems);
  }
  getItemPaged(page:number, size:number): Observable<ItemPage> {
    const params = new HttpParams() 
      .set('page', page)
      .set('size', size);

    return this.http.get<ItemPage>(this.urlItems, { params });
  } 

  deleteItemById(id:number):Observable<void>{
    return this.http.delete<void>(`${this.urlItems}/${id}`);
  }
  saveItem(newItem:Item):Observable<Item>{
    return this.http.post<Item>(this.urlItems,newItem);
  }
  findItemByCategory(categoryName:string):Observable<Item[]>{
    const params: HttpParams = new HttpParams().append('categoryName', categoryName);
    return this.http.get<Item[]>(this.urlItems, { params });
  }
  findItemByName(name:string):Observable<Item[]>{
    const params:HttpParams=new HttpParams().append('name',name);
    return this.http.get<Item[]>(this.urlItems,{params});
  }
  updateItem(editedItem:Item):Observable<Item>{
    return this.http.put<Item>(this.urlItems+'/'+editedItem.id,editedItem);
  }
}
