import { Component, Input, OnInit, Output,  EventEmitter } from '@angular/core';
import { Item } from '../model/item.model';

@Component({
  selector: 'tr[app-item-edit]',
  templateUrl: './item-edit.component.html',
  styleUrls: ['./item-edit.component.css']
})
export class ItemEditComponent implements OnInit {
  @Input()
  item:Item;

  @Input()
  itemToEdit:Item;

  @Output()
  ItemDeleted:EventEmitter<number>=new EventEmitter();

  @Output()
  ItemSetToEdit: EventEmitter<Item>=new EventEmitter();

  constructor() { 
    this.item=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
    this.itemToEdit=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
  }

  ngOnInit(): void {
  }
  deleteItem(){
    this.ItemDeleted.next(this.item.id);
  }
  setItemToEdit(){
    this.ItemSetToEdit.next(this.item);
    console.log("item-edit");
    console.log(this.item);
  }
}
