import { Component, OnInit } from '@angular/core';
import { ItemService } from '../item.service';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-menu-page',
  templateUrl: './menu-page.component.html',
  styleUrls: ['./menu-page.component.css']
})
export class MenuPageComponent implements OnInit {
  items:Item[];
  public last=false;
  public page=0;
  public size=5;

  constructor(
    private itemService:ItemService
  ) { 
    this.items=[];
  }

  ngOnInit(): void {
    this.getItems();
  }
  getItems(){
    this.itemService.getItemPaged(this.page,this.size)
    .subscribe(res=>{
      this.items=res.content;
      this.last=res.last
    });
  }
  findItemByCategory(categoryName:string){
    this.itemService.findItemByCategory(categoryName)
    .subscribe(res=>this.items=res)
  }
  nextPage(){
    this.page++;
    this.getItems();
  }
  previousPage(){
    this.page--;
    this.getItems();
  }
  changePageSize(){
    this.page=0;
    this.getItems();
  }
}
