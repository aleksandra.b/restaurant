import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-item-list-for-edit',
  templateUrl: './item-list-for-edit.component.html',
  styleUrls: ['./item-list-for-edit.component.css']
})
export class ItemListForEditComponent implements OnInit {
  @Input()
  items:Item[];


  @Output()
  ItemDeleted:EventEmitter<number>=new EventEmitter();

  @Output()
  ItemSetToEdit: EventEmitter<Item>=new EventEmitter();


  constructor() { 
    this.items=[];
  }

  ngOnInit(): void {
  }
  deleteItem(id:number){
    this.ItemDeleted.next(id);
  }
  setItemToEdit(item:Item){
    this.ItemSetToEdit.next(item);
  }
}
