import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemListForEditComponent } from './item-list-for-edit.component';

describe('ItemListForEditComponent', () => {
  let component: ItemListForEditComponent;
  let fixture: ComponentFixture<ItemListForEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemListForEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListForEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
