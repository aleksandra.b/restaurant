import { Component, Input, OnInit } from '@angular/core';
import { Item } from '../model/item.model';

@Component({
  selector: 'tr[app-item]',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input()
  public item:Item;

  constructor() { 
    this.item=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
  }

  ngOnInit(): void {
  }


}
