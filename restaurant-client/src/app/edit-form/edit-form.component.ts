import { Component, Input, OnInit, Output ,EventEmitter} from '@angular/core';

import { Category } from '../model/category.model';
import { Item } from '../model/item.model';

@Component({
  selector: 'app-edit-form',
  templateUrl: './edit-form.component.html',
  styleUrls: ['./edit-form.component.css']
})
export class EditFormComponent implements OnInit {
  @Input()
  itemToEdit:Item;

  @Output()
  itemIsEdited:EventEmitter<Item>=new EventEmitter();

  @Output()
  isForEdited:EventEmitter<boolean>=new EventEmitter();
  
  @Input()
  categories:Category[];

  constructor() { 
    this.categories=[];
  }

  ngOnInit(): void {
  }
  editItem(){
    this.itemIsEdited.next(this.itemToEdit);
    this.resetItemToEdit();
  }
  resetItem(){
    this.resetItemToEdit();
    this.isForEdited.next();
  }
  resetItemToEdit(){
    this.itemToEdit=new Item({
      id:0,
      name:'',
      price:0,
      category:{
        id:0,
        name:''
      }
    });
  }
}
