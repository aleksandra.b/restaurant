package vp.restaurant.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import vp.restaurant.model.Category;
import vp.restaurant.model.Item;
@Repository
public interface ItemRepository extends JpaRepository<Item, Long>{
	List<Item> findByCategory(Category category);
	List<Item> findByName(String name);
}
