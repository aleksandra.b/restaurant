package vp.restaurant.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.restaurant.model.Category;
import vp.restaurant.model.Item;
import vp.restaurant.repository.ItemRepository;

@Component
public class ItemService {
	@Autowired
	private ItemRepository itemRepository;
	
	public List<Item> findAll() {
		return this.itemRepository.findAll();
	}
	public Page<Item> findAll(Pageable page) {
		return itemRepository.findAll(page);
	}
	public Optional<Item> findById(Long id) {
		return this.itemRepository.findById(id);
	}
	public void deleteById(Long id) {
		this.itemRepository.deleteById(id);
	}
	public void delete(Item item) {
		this.itemRepository.delete(item);
	}
	public Item save(Item newItem) {
		return this.itemRepository.save(newItem);
	}
	public List<Item> findByCategory(Category category){
		return this.itemRepository.findByCategory(category);
	}
	public List<Item> findByName(String name) {
		return this.itemRepository.findByName(name);
	}
}
