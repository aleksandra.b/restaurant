package vp.restaurant.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import vp.restaurant.repository.CategoryRepository;
import vp.restaurant.model.*;

@Component
public class CategoryService {
	@Autowired
	private CategoryRepository categoryRepository;
	
	public List<Category> findAll() {
		return this.categoryRepository.findAll();
	}
	public Optional<Category> findById(Long id) {
		return this.categoryRepository.findById(id);
	}
	public void delete(Long id) {
		this.categoryRepository.deleteById(id);
	}
	public Category save(Category newCategory) {
		return this.categoryRepository.save(newCategory);
	}
	public Category findByName(String name) {
		return this.categoryRepository.findByName(name);
	}
}
