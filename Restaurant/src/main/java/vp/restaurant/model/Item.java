package vp.restaurant.model;

import javax.persistence.*;

@Entity
public class Item {
	@Id
	@GeneratedValue
	private Long id;
	
	@Column
	private String name;
	
	@Column 
	private double price;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Item(Long id, String name, double price, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.category = category;
	}
	public Item() {}
}
