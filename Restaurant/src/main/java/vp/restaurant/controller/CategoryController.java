package vp.restaurant.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import vp.restaurant.dto.CategoryDTO;
import vp.restaurant.model.Category;
import vp.restaurant.service.CategoryService;

@CrossOrigin("*")
@RestController
public class CategoryController {
	@Autowired
	CategoryService categoryService;
	
	@RequestMapping(value="api/categories",method = RequestMethod.GET,produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<List<CategoryDTO>> findAll(){
		List<Category> categories=categoryService.findAll();
		List<CategoryDTO> categoriesDTO=new ArrayList<>();
		for (Category category : categories) {
			categoriesDTO.add(new CategoryDTO(category));
		}
		return new ResponseEntity<>(categoriesDTO,HttpStatus.OK);
	}
}
