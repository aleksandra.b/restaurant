package vp.restaurant.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.restaurant.dto.ItemDTO;
import vp.restaurant.dto.ItemPageDTO;
import vp.restaurant.model.Category;
import vp.restaurant.model.Item;
import vp.restaurant.repository.ItemRepository;
import vp.restaurant.service.CategoryService;
import vp.restaurant.service.ItemService;
@CrossOrigin("*")
@RestController
public class ItemController {
	@Autowired
	ItemService itemService;
	@Autowired
	CategoryService categoryService;
	
//	@RequestMapping(value="api/items",method = RequestMethod.GET,produces = {
//			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
//	public ResponseEntity<List<ItemDTO>> findAll(){
//		List<Item> items=itemService.findAll();
//		List<ItemDTO> itemsDto=new ArrayList<>();
//		for (Item item : items) {
//			itemsDto.add(new ItemDTO(item));
//		}
//		return new ResponseEntity<>(itemsDto,HttpStatus.OK);
//	}
	@RequestMapping(value="api/items", params= {"page","size"},method = RequestMethod.GET,produces = {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
	public ResponseEntity<ItemPageDTO> findAll(Pageable pageable){
		Page<Item> items=itemService.findAll(pageable);
		ItemPageDTO pageDTO=new ItemPageDTO(dtosFromItems(items.getContent()), items.isLast());
		return new ResponseEntity<>(pageDTO,HttpStatus.OK);
	}
    private List<ItemDTO> dtosFromItems(List<Item> items) {
    	ArrayList<ItemDTO> dtos = new ArrayList<>();
        for (Item item: items) {
        	dtos.add(new ItemDTO(item));
        }
        
        return dtos;
    }
	
	@RequestMapping(value="api/items",method = RequestMethod.POST,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> save(@RequestBody ItemDTO newItemDTO){
		Item item=new Item(newItemDTO.getId(), newItemDTO.getName(),newItemDTO.getPrice(), newItemDTO.getCategory());
		Item savedItem=this.itemService.save(item);
		return new ResponseEntity<>(new ItemDTO(savedItem),HttpStatus.CREATED);
	}
	
	@RequestMapping(value="api/items/{id}",method = RequestMethod.PUT,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ItemDTO> update(@RequestBody ItemDTO editedItemDTO,@PathVariable Long id){
		Optional<Item> itemOptionals=this.itemService.findById(id);
		
		if(!itemOptionals.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		Item item=itemOptionals.get();
		item.setId(id);
		item.setCategory(editedItemDTO.getCategory());
		item.setName(editedItemDTO.getName());
		item.setPrice(editedItemDTO.getPrice());
		
		final Item savedItem=this.itemService.save(item);
		
		
		return new ResponseEntity<>(new ItemDTO(savedItem),HttpStatus.OK);
	}
	
	@RequestMapping(value="api/items/{id}",method = RequestMethod.GET,produces= {
			MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ItemDTO> getById(@PathVariable Long id){
		Optional<Item> item=itemService.findById(id);
		
		if(!item.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		
		ItemDTO itemDto=new ItemDTO(item.get());
		return new ResponseEntity<>(itemDto,HttpStatus.OK);
	}
	@RequestMapping(value = "api/items/{id}",method = RequestMethod.DELETE)
	public ResponseEntity delete(@PathVariable Long id){
		Optional<Item> itemOption=this.itemService.findById(id);
		if(!itemOption.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		this.itemService.delete(itemOption.get());
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@RequestMapping(value="api/items", method = RequestMethod.GET,
			params = "categoryName",
			produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<List<ItemDTO>> findByCategory(@RequestParam String categoryName){
		Category category=this.categoryService.findByName(categoryName);
		List<Item> items=this.itemService.findByCategory(category);
		List<ItemDTO> itemsDTO=new ArrayList<>();
		for (Item item : items) {
			itemsDTO.add(new ItemDTO(item));
		}
		return new ResponseEntity<>(itemsDTO,HttpStatus.OK);
	}
	@RequestMapping(value="api/items", method = RequestMethod.GET,
			params = "name",
			produces= {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<List<ItemDTO>> findByName(@RequestParam String name){
		List<Item> items=this.itemService.findByName(name);
		List<ItemDTO> itemsDTO=new ArrayList<>();
		for (Item item : items) {
			itemsDTO.add(new ItemDTO(item));
		}
		return new ResponseEntity<>(itemsDTO,HttpStatus.OK);
	}
	
}
