package vp.restaurant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication
public class App {
	// odkomentarisati ovo ako nemam podatke u bazi podataka
//	@Autowired
//	InitialData initialData;
	
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
