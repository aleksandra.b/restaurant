package vp.restaurant.dto;

import java.util.List;

public class ItemPageDTO {

	List<ItemDTO> content;
	boolean last;
	
	public List<ItemDTO> getContent() {
		return content;
	}
	public void setContent(List<ItemDTO> content) {
		this.content = content;
	}
	public boolean isLast() {
		return last;
	}
	public void setLast(boolean last) {
		this.last = last;
	}
	public ItemPageDTO(List<ItemDTO> content, boolean last) {
		super();
		this.content = content;
		this.last = last;
	}
	
	
}
