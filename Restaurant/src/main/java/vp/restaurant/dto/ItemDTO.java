package vp.restaurant.dto;

import vp.restaurant.model.Category;
import vp.restaurant.model.Item;

public class ItemDTO {
	private Long id;
	private String name;
	private double price;
	private Category category;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public ItemDTO(Long id, String name, double price, Category category) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.category = category;
	}
	public ItemDTO(Item newItem) {
		this.id=newItem.getId();
		this.name=newItem.getName();
		this.price=newItem.getPrice();
		this.category=newItem.getCategory();
	}
	
}
